# USAGE
# python deep_learning_with_opencv.py --image images/jemma.png --prototxt bvlc_googlenet.prototxt --model bvlc_googlenet.caffemodel --labels synset_words.txt

# import the necessary packages
import numpy as np
import argparse
import time
import cv2
from threading import Thread, Lock







class WebcamVideoStream :
    def __init__(self, src = 0, width = 1280, height = 720) :
        self.stream = cv2.VideoCapture(src)
        self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        (self.grabbed, self.frame) = self.stream.read()
        self.started = False
        self.read_lock = Lock()

    def start(self) :
        if self.started :
            print "already started!!"
            return None
        self.started = True
        self.thread = Thread(target=self.update, args=())
        self.thread.start()
        return self

    def update(self) :
        while self.started :
            (grabbed, frame) = self.stream.read()
            self.read_lock.acquire()
            self.grabbed, self.frame = grabbed, frame
            self.read_lock.release()

    def read(self) :
        self.read_lock.acquire()
        frame = self.frame.copy()
        self.read_lock.release()
        return frame

    def stop(self) :
        self.started = False
        self.thread.join()

    def __exit__(self, exc_type, exc_value, traceback) :
        self.stream.release()











image = 'images/eagle.png'
prototxt = 'bvlc_googlenet.prototxt'
model = 'bvlc_googlenet.caffemodel'
labels = 'synset_words.txt'

# load the input image from disk
image = cv2.imread(image)


# load the class labels from disk
rows = open(labels).read().strip().split("\n")
classes = [r[r.find(" ") + 1:].split(",")[0] for r in rows]

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(prototxt, model)

vs = WebcamVideoStream().start()
while True :
	image = vs.read()

	height, width = image.shape[:2]
	print(width)
	print(height)

	blob = cv2.dnn.blobFromImage(image, 1, (224, 224), (104, 117, 123))


	# set the blob as input to the network and perform a forward-pass to
	# obtain our output classification
	net.setInput(blob)
	start = time.time()
	preds = net.forward()
	end = time.time()
	print("[INFO] classification took {:.5} seconds".format(end - start))

	# sort the indexes of the probabilities in descending order (higher
	# probabilitiy first) and grab the top-5 predictions
	idxs = np.argsort(preds[0])[::-1][:5]

	# loop over the top-5 predictions and display them
	for (i, idx) in enumerate(idxs):
		# draw the top prediction on the input image
		if i == 0:
			text = "Label: {}, {:.2f}%".format(classes[idx],
				preds[0][idx] * 100)
			cv2.putText(image, text, (5, 25),  cv2.FONT_HERSHEY_SIMPLEX,
				0.7, (0, 0, 255), 2)

		# display the predicted label + associated probability to the
		# console	
		print("[INFO] {}. label: {}, probability: {:.5}".format(i + 1,
			classes[idx], preds[0][idx]))

	# display the output image
	#cv2.imshow("Image", image)
	#cv2.waitKey(0)



	cv2.imshow('webcam', image)
	if cv2.waitKey(1) == 27 :
		break

vs.stop()
cv2.destroyAllWindows()

	

	